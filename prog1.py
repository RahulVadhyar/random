import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv("airline-passengers.csv")
df.describe()

df.info()

#add missing values
df.iloc[10:15, df.columns.get_loc("Passengers")] = np.nan
df.iloc[50:55, df.columns.get_loc("Passengers")] = np.nan
df.iloc[70:74, df.columns.get_loc("Passengers")] = np.nan
df.iloc[81:86, df.columns.get_loc("Passengers")] = np.nan
df.iloc[100:110, df.columns.get_loc("Passengers")] = np.nan

missing_values = df['Passengers'].isnull().sum()
print(f"Number of missing values = {missing_values} which is {missing_values*100/len(df)}% of dataset")

df["Month"] = pd.to_datetime(df["Month"], format="%Y-%m")
df.set_index("Month", inplace=True)

#interpolate missing values
df["Passengers"] = df["Passengers"].interpolate(method="cubic")

plt.figure(figsize=(10, 10))
plt.plot(df.index, df["Passengers"])
plt.title("Airline Passengers data")
plt.xlabel("Time")
plt.ylabel("Number of passengers")


#try multiplicative
from statsmodels.tsa.seasonal import seasonal_decompose
result = seasonal_decompose(df["Passengers"], model = "multiplicative")
result.plot()
plt.show()

#try additive
from statsmodels.tsa.seasonal import seasonal_decompose
result = seasonal_decompose(df["Passengers"], model = "additive")
result.plot()
plt.show()