import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.ar_model import AutoReg

df = pd.read_csv("data.csv")
df.describe()

df['x'].plot()

plot_acf(df["x"])

plot_pacf(df["x"])