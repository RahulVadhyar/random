import numpy
import matplotlib.pyplot as plt
import pandas as pd

samples = numpy.random.normal(0, 1, size=1000)
plt.plot(samples)
plt.title("White Noise data")
plt.show()

df = pd.read_csv("airline-passengers.csv")
plt.plot(df['Passengers'])
plt.title("Passengers Data")
plt.show()

from statsmodels.tsa.stattools import adfuller, kpss
results = adfuller(df['Passengers'])

print("Augmented Dickey Fuller test")
print(f"Test statistic is {results[0]}")
print(f"P value is {results[1]}")

results = kpss(df['Passengers'])
print("Kwiatkowski-Phillips-Schmidt-Shin test")
print(f"Test statistic is {results[0]}")
print(f"P value is {results[1]}")