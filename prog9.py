import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from arch import arch_model
import yfinance as yf
from sklearn.model_selection import train_test_split

stock_data = yf.download('AAPL', start='2010-01-01', end='2023-01-01')

returns = np.exp(np.log(stock_data['Adj Close']).diff()).dropna()

returns.plot()

plot_acf(returns)

train_size = int(len(returns) * 0.8)
train, test = returns[:train_size], returns[train_size:]

model = arch_model(train, vol='ARCH', p=8)
model_fitted = model.fit(disp='off')

model_fitted.summary()

forecast_horizon = len(test)
forecast = model_fitted.forecast(horizon=forecast_horizon)

test = pd.DataFrame(test)
test["Forecast"] = forecast.mean.values[0]

plt.plot(train, label = "Train")
plt.plot(test["Adj Close"], label = "Test")
plt.plot(test["Forecast"], label = "Forecast")
plt.xlabel("Date")
plt.ylabel("Returns")
plt.title("ARCH Forecast for AAPL stock")
plt.legend()
plt.show()