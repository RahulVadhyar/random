import numpy as np
import pandas as pd
from statsmodels.tsa.holtwinters import SimpleExpSmoothing, Holt, ExponentialSmoothing
import matplotlib.pyplot as plt

TRAIN_SPLIT = 0.8

df = pd.read_csv("apples.csv")
df.set_index('Year', inplace=True)
df.info()
train_size = int(0.8*len(df))
train, test = df.iloc[:train_size], df.iloc[train_size:]
print(f"Length of entire dataset: {len(df)}")
print(f"Length of train and test split are {len(train)} and {len(test)}")

def plot_model(train, test, predictions, method):
    plt.figure(figsize = (12, 6))
    plt.plot(train.index, train["Value"], label = "train")
    plt.plot(test.index, test["Value"], label = "actual")
    plt.plot(predictions.index, predictions["Value"], label = f"{method}")
    plt.title(f"{method} forecasting")
    plt.xlabel("Date")
    plt.ylabel("Value")
    plt.legend()
    plt.show()
    
def evaluate_model(test, predictions, method):
    mse = ((test["Value"] - predictions["Value"])**2).mean()
    mae = np.abs(test["Value"] - predictions["Value"]).mean()
    rmse = mse**0.5
    print(f"Evaluation of model {method}")
    print(f"MSE : {mse}")
    print(f"MAE : {mae}")
    print(f"RMSE : {rmse}")
    print("")
    
window = 5
sma_predictions = pd.concat([train.iloc[-1:], test]).rolling(window).mean().iloc[1:]
plot_model(train, test, sma_predictions, "Simple Moving Average")
evaluate_model(test, sma_predictions, "Simple Moving Average")

smoothing_levels = [0.2, 0.5, 0.7]
for smoothing_level in smoothing_levels:
    model = SimpleExpSmoothing(train["Value"].values).fit(smoothing_level = smoothing_level)
    ses_output = model.forecast(len(test))
    ses_preds = test.copy()
    ses_preds["Value"] = ses_output
    plot_model(train, test, ses_preds, f"Simple Exponential Smoothing({smoothing_level})")
    evaluate_model(test, ses_preds, f"Simple Exponential Smoothing({smoothing_level})")
    
#Holt Winters Smoothing
seasonal_periods = [3, 5, 7, 9]
for seasonal_period in seasonal_periods:
    model = ExponentialSmoothing(train["Value"].values, seasonal_periods=seasonal_period, trend='mul', seasonal='mul').fit()
    es_output = model.forecast(len(test))
    es_preds = test.copy()
    es_preds["Value"] = es_output
    plot_model(train, test, es_preds, f"Holt Winters({seasonal_period})")
    evaluate_model(test, es_preds, f"Holt Winters({seasonal_period})")