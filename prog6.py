import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima.model import ARIMA

df = pd.read_csv("data.csv")
df.describe()

df['x'].plot()

plot_acf(df["x"])

plot_pacf(df["x"])

results = ARIMA(df["x"], order=(0, 0, 1)).fit()
results.summary()

plt.plot(df["x"], label='Original Data')
plt.plot(results.fittedvalues, color='red', label='Fitted Values')
plt.legend()
plt.title('MA(1) Model Fitted Values vs. Original Data')
plt.show()

results = ARIMA(df["x"], order=(0, 0, 5)).fit()
results.summary()

plt.plot(df["x"], label='Original Data')
plt.plot(results.fittedvalues, color='red', label='Fitted Values')
plt.legend()
plt.title('MA(5) Model Fitted Values vs. Original Data')
plt.show()