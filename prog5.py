import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.ar_model import AutoReg

df = pd.read_csv("data.csv")
df.describe()

plot_acf(df["x"])

plot_pacf(df["x"])

results = AutoReg(df["x"], lags=1).fit()
results.summary()

plt.plot(df["x"], label='Original Data')
plt.plot(results.fittedvalues, color='red', label='Fitted Values')
plt.legend()
plt.title('AR(1) Model Fitted Values vs. Original Data')
plt.show()

results = AutoReg(df["x"], lags=5).fit()
results.summary()

plt.plot(df["x"], label='Original Data')
plt.plot(results.fittedvalues, color='red', label='Fitted Values')
plt.legend()
plt.title('AR(5) Model Fitted Values vs. Original Data')
plt.show()